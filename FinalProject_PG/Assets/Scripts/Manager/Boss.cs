﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    [SerializeField] private BossSpaceShip bossSpaceship;
    [SerializeField] private int bossSpaceshipHp;
    [SerializeField] private int bossSpaceshipMoveSpeed;
    private void Awake()
    {
    
        Debug.Assert(bossSpaceship != null, "enemySpaceship cannot be null");
        Debug.Assert(bossSpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
        Debug.Assert(bossSpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
    

    }
    public void Start()
    {
        SpawnBossSpaceship();
    }

    private void SpawnBossSpaceship()
    {
        var spawnedBossShip = Instantiate(bossSpaceship);
        spawnedBossShip.Init(bossSpaceshipHp, bossSpaceshipMoveSpeed);
        spawnedBossShip.OnExploded += OnBossSpaceshipExploded;
    }

    private void OnBossSpaceshipExploded()
    {
        DestroyRemainingShips();
        SceneManager.LoadScene("Game");
        SpawnBossSpaceship();
        

    }
    private void DestroyRemainingShips()
    {
        var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in remainingEnemy)
        {
            Destroy(enemy);
        }
        var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
        foreach (var player in remainingPlayer)
        {
            Destroy(player);
        }            
    }
}


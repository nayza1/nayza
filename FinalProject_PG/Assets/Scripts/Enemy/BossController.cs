﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [SerializeField] private BossSpaceShip bossSpaceship;
    [SerializeField] private float chasingThresholdDistance;

    private PlayerSpaceship spawnedPlayerShip;

    private void Start()
    {
        spawnedPlayerShip = GameManager.Instance.spawnedPlayerShip;
    }

    private void Update()
    {
        MoveToPlayer();
        bossSpaceship.Fire();
    }

    private void MoveToPlayer()
    {
        // TODO: Implement this later
        if (spawnedPlayerShip == null)
            return;
        var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
        if (distanceToPlayer < chasingThresholdDistance)
        {
            var direction = (Vector2) (spawnedPlayerShip.transform.position - transform.position);
            direction.Normalize();
            var distance = direction * bossSpaceship.Speed * Time.deltaTime;
            gameObject.transform.Translate(distance);
        }
    }
}
